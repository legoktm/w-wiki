w-wiki
============
[![crates.io](https://img.shields.io/crates/v/w-wiki.svg)](https://crates.io/crates/w-wiki)
[![docs.rs](https://docs.rs/w-wiki/badge.svg)](https://docs.rs/w-wiki)
[![pipeline status](https://gitlab.com/legoktm/w-wiki/badges/master/pipeline.svg)](https://gitlab.com/legoktm/w-wiki/-/commits/master)
[![coverage report](https://gitlab.com/legoktm/w-wiki/badges/master/coverage.svg)](https://legoktm.gitlab.io/w-wiki/coverage/)

The `w-wiki` crate helps shorten URLs using the [`w.wiki`](https://w.wiki/)
service. See the [URL shortener documentation](https://meta.wikimedia.org/wiki/Wikimedia_URL_Shortener)
for more details.

## License
w-wiki is (C) 2020-2021 Kunal Mehta, released under the GPLv3 or any later version, see COPYING for details.
